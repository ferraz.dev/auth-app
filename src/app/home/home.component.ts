import { Component, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { User } from '../auth/services/user';
import { SubscriptionLike } from 'rxjs';
import { MatSnackBarRef, SimpleSnackBar, MatSnackBar } from '@angular/material';
import { AuthService } from '../auth/services/auth.service';
import { Router } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { ScreenWatchingComponent } from '../core/screen-watching-component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends ScreenWatchingComponent
  implements OnDestroy {
  user: User;
  userSub: SubscriptionLike;
  snackbar: MatSnackBarRef<SimpleSnackBar>;

  navList: { name: string; path: string }[] = [
    { name: 'Home', path: '' },
    { name: 'My account', path: 'account' }
  ];

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,

    authService: AuthService,
    snackbar: MatSnackBar,
    router: Router
  ) {
    super(changeDetectorRef, media);
    this.userSub = authService.user.subscribe((u: User) => {
      if (u) {
        this.user = u;
        if (this.snackbar) this.snackbar.dismiss();
      } else {
        this.snackbar = snackbar.open(
          'It seems you are not logged in anymore',
          'Login'
        );
        this.snackbar.onAction().subscribe(() => {
          router.navigateByUrl('/auth/login');
        });
      }
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.userSub.unsubscribe();
    if (this.snackbar) this.snackbar.dismiss();
  }
}
