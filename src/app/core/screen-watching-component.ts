import { OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

export class ScreenWatchingComponent implements OnDestroy {
  smQuery: MediaQueryList;
  mdQuery: MediaQueryList;
  private _queryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this._queryListener = () => changeDetectorRef.detectChanges();

    this.smQuery = media.matchMedia('(min-width: 600px)');
    this.smQuery.addListener(this._queryListener);

    this.mdQuery = media.matchMedia('(min-width: 960px)');
    this.mdQuery.addListener(this._queryListener);
  }

  ngOnDestroy() {
    this.smQuery.removeListener(this._queryListener);
    this.mdQuery.removeListener(this._queryListener);
  }
}
