import { Component, OnInit, Input } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/animations';

export interface Requirement {
  message: string;
  fulfilled: () => boolean;
}

@Component({
  selector: 'app-requirements',
  templateUrl: './requirements.component.html',
  styleUrls: ['./requirements.component.scss'],
  animations: [
    trigger('collapsible', [
      state('expanded', style({ height: '*' })),
      state('collapsed', style({ height: '0' })),
      transition('expanded <=> collapsed', animate('300ms ease-in-out'))
    ])
  ]
})
export class RequirementsComponent implements OnInit {
  @Input()
  visible = true;
  @Input()
  list: Requirement[] = [];
  constructor() {}

  ngOnInit() {}
}
