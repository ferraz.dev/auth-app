import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatInputModule,
  MatFormFieldModule,
  MatButtonModule,
  MatProgressBarModule
} from '@angular/material';

import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { AuthProvidersModule } from './services/auth-providers.module';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { RequirementsModule } from '../core/requirements/requirements.module';
import { UnauthGuard } from './services/unauth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [UnauthGuard] },
  { path: 'signup', component: SignupComponent, canActivate: [UnauthGuard] },
  {
    path: 'recover',
    component: RecoverPasswordComponent,
    canActivate: [UnauthGuard]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RequirementsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatProgressBarModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    AuthProvidersModule
  ],
  declarations: [LoginComponent, SignupComponent, RecoverPasswordComponent]
})
export class AuthModule {}
