import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { User } from 'firebase';
import { MatSnackBar } from '@angular/material';
import { Requirement } from 'src/app/core/requirements/requirements.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  isLoading = false;
  constructor(
    formBuilder: FormBuilder,
    private authService: AuthService,
    router: Router,
    snackbar: MatSnackBar
  ) {
    this.form = formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });

    authService.user.subscribe((user: User) => {
      if (user) {
        snackbar.open('Successfully logged in', 'Dismiss', { duration: 5000 });
        router.navigateByUrl('/home');
      }
    });
  }
  get email(): AbstractControl {
    return this.form.get('email');
  }

  emailRequirements: Requirement[] = [
    {
      message: 'Check the email format',
      fulfilled: () =>
        !(this.email.errors && this.email.errors.email) && this.email.value
    }
  ];

  ngOnInit() {}

  async submit() {
    if (!this.form.valid) {
      console.log('Form is not valid');
      return;
    }
    this.isLoading = true;
    const success = await this.authService.login(
      this.email.value,
      this.form.get('password').value
    );
    if (success) {
      console.log('Logged in successfully, wait for redirect...');
    } else {
      this.isLoading = false;
    }
  }
}
