import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors
} from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';
import { Requirement } from 'src/app/core/requirements/requirements.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnDestroy {
  form: FormGroup;
  isLoading: boolean = false;
  private userSub: Subscription;

  constructor(
    private authService: AuthService,
    formBuilder: FormBuilder,
    router: Router,
    snackbar: MatSnackBar
  ) {
    this.buildForm(formBuilder);
    this.userSub = authService.user.subscribe(user => {
      if (user) {
        snackbar.open('Successfully logged in', 'Dismiss', { duration: 5000 });
        router.navigateByUrl('/home');
      }
    });
  }
  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

  private buildForm(formBuilder: FormBuilder) {
    this.form = formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: [
          '',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.pattern(/[0-9]/),
            Validators.pattern(/[a-z]/),
            Validators.pattern(/[A-Z]/)
          ]
        ],
        confirmPassword: ['', [Validators.required]]
      },
      {
        validator: [this.matchPasswordValidator]
      }
    );
  }

  get email(): AbstractControl {
    return this.form.get('email');
  }
  get password(): AbstractControl {
    return this.form.get('password');
  }
  get confirmPassword(): AbstractControl {
    return this.form.get('confirmPassword');
  }

  emailRequirements: Requirement[] = [
    {
      message: 'Check the email format',
      fulfilled: () =>
        !(this.email.errors && this.email.errors.email) && this.email.value
    }
  ];
  passwordRequirements: Requirement[] = [
    {
      message: 'The password must have at least 6 characters',
      fulfilled: () =>
        !(this.password.errors && this.password.errors.minlength) &&
        this.password.value
    },
    {
      message: 'The password must have numbers',
      fulfilled: () => /[0-9]/.test(this.password.value)
    },
    {
      message: 'The password must have lowercase letters',
      fulfilled: () => /[a-z]/.test(this.password.value)
    },
    {
      message: 'The password must have uppercase letters',
      fulfilled: () => /[A-Z]/.test(this.password.value)
    }
  ];
  formRequirements: Requirement[] = [
    {
      message: 'Passwords must match',
      fulfilled: () => !(this.form.errors && this.form.errors.passwordMatch)
    }
  ];

  private matchPasswordValidator(group: AbstractControl): ValidationErrors {
    let ok: boolean =
      group.get('password').value === group.get('confirmPassword').value;
    return ok ? null : { passwordMatch: true };
  }

  async submit() {
    if (!this.form.valid) {
      console.log('Form is not valid');
      return;
    }
    this.isLoading = true;
    const success = await this.authService.signup(
      this.email.value,
      this.password.value
    );
    if (success) {
      console.log('Successfully registered user');
    } else {
      this.isLoading = false;
    }
  }
}
