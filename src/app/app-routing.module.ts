import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnauthGuard } from './auth/services/unauth.guard';
import { AuthGuard } from './auth/services/auth.guard';
import { AuthProvidersModule } from './auth/services/auth-providers.module';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
    canLoad: [UnauthGuard]
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule',
    canLoad: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    loadChildren:
      './core/page-not-found/page-not-found.module#PageNotFoundModule'
  }
];

@NgModule({
  imports: [AuthProvidersModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
